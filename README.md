﻿# DocInfoFileLinks

## Component Information
* Created by: Mythics
* Author: Jonathan Hult
* Last Updated: build_3_20140318
* License: MIT

## Overview
This WebCenter Content component can hide the "Links" area on Content Information pages. This functionality is controlled by the preference prompt DocInfoFileLinks_Options. 0 means the "Links" will be displayed as normal. 1 means this area will always be hidden. 2 means this area will only be hidden if the content item is metadata only. This component can also shorten the Web Location and Native File URL display. This functionality is controlled by the preference prompt DocInfoFileLinks_ShortenURLs.

* Dynamichtml includes:
	- std_docinfo_links: Core - Override to hide "Links" area on Content Information pages. This is controlled by the value in the preference prompt DocInfoFileLinks_Options.
	- docinfo_url_doc_link: Core - Override to shorten Web Location URL display. This is enabled/disabled via the value in the preference prompt DocInfoFileLinks_ShortenURLs.
	- docinfo_download_native_link: Core - Override to shorten Native File URL display. This is enabled/disabled via the value in the preference prompt DocInfoFileLinks_ShortenURLs.
	- doc_file_get_copy: Core - Override to shorten Native File URL display. This is enabled/disabled via the value in the preference prompt DocInfoFileLinks_ShortenURLs.

* Strings:
	- DocInfoFileLinks_WebLocation
	- DocInfoFileLinks_NativeFile
	
* Preference prompts:
	- DocInfoFileLinks_Options: 0 = component disabled; 1 = always hide; 2 = only hide for metadata only content items
	- DocInfoFileLinks_ShortenURLs: Shorten Web Location and Native File URL display?

## Compatibility
This component was built upon and tested on the version listed below, this is the only version it is known to work on, but it may work on older/newer versions.

* 11.1.1.8.1DEV-2014-01-06 04:18:30Z-r114490 (Build:7.3.5.185)

## Changelog
* build_3_20140318
	- Renamed component from HideDocInfoFileLinks to DocInfoFileLinks to properly reflect its behavior
	- Changed Web Location and Native File URL display to use custom Strings
* build_2_20140317
	- Fixed issue with shortening web-viewable file name
* build_1_20140316
	- Initial component release